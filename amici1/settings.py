# -*- coding: UTF-8 -*-
import os
from lino_amici.lib.amici.settings import *

import django_heroku

s = globals()

class Site(Site):
    title = "lino-amici"

    never_build_site_cache = True

    is_demo_site = True

    languages = 'en de fr bn'

    use_linod = False

    def get_plugin_configs(self):
        yield super(Site, self).get_plugin_configs()

SITE = Site(s)
BASE_DIR = str(SITE.cache_dir)

DEBUG = True
ALLOWED_HOSTS = ['localhost', 'lino-amici.herokuapp.com']

WHITENOISE_MANIFEST_STRICT = False

EMAIL_SUBJECT_PREFIX = '[amici] '

s['MIDDLEWARE'] += ('whitenoise.middleware.WhiteNoiseMiddleware',)

django_heroku.settings(locals())

s['DATABASES']['default']['ENGINE'] = 'django.db.backends.postgresql'
